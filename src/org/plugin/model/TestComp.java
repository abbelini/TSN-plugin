package org.plugin.model;

import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;

public class TestComp extends Composite {

	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Text txtHi;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public TestComp(Composite parent, int style) {
		super(parent, style);
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
			}
		});
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		
		txtHi = new Text(this, SWT.BORDER);
		txtHi.setText("hi");
		txtHi.setBounds(0, 0, 73, 21);
		toolkit.adapt(txtHi, true, true);

	}

}
