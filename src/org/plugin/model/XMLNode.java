package org.plugin.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;



public class XMLNode extends Object{
	public String string;
	public ArrayList<String> attributes;
	public ArrayList<String> attributesvalues;
	public ArrayList<XMLNode> children;
	public boolean isComment=false;
	public XMLNode(String text){
		string = text;
		attributes = new ArrayList<>();
		attributesvalues = new ArrayList<>();
		children = new ArrayList<>();
	}
	public boolean isLeaf(){
		return children.isEmpty();
	}
	
	public String toString() { 
		String out = string;

		/*
		for (int i = 0; i < attributes.size(); i++) {
			out += " " + attributes.get(i) + "=\"" + attributesvalues.get(i) + "\"";	
		}
		if(isComment)
			out = "<!-- " + out + " -->";
		 */
	    return out;
	}
	
	public void addAttributes(String name, String value){
		attributes.add(name);
		attributesvalues.add(value);
	}
	public void add(XMLNode node){
		children.add(node);
	}
	public void add(XMLNode[] nodes){
		for (XMLNode xmlNode : nodes) {
			children.add(xmlNode);
		}
	}
	
	public XMLNode get(int index){
		return children.get(index);
	}
	
	public XMLNode getByName(String name){
		for (XMLNode node : children) {
			if(node.string.equals(name)){
				return node; // Return on first match
			}
		}
		return null;
	}
	
	public ArrayList<XMLNode> getAllWithName(String name){
		ArrayList<XMLNode> list = new ArrayList<>();
		for (XMLNode node : children) {
			if(node.string.equals(name)){
				list.add(node);
			}
		}
		return list;
	}
	
	public int getAttributIndex(String attribute) {
		for(int i=0; i<attributes.size();i++) {
			if(attributes.get(i).equals(attribute)) {return i;}
		}
		return -1; // Failed to find attribute
	}
	
	// returns the attribute by name
	public String getAttributeByName(String name){
		return this.attributesvalues.get(this.getAttributIndex(name));
	}
	
	public Object convertToElement(Document dom){
		Element e = null;
		try{
		// Is this a textnode or commentnode?
		if(attributes.size()+children.size()==0){
			if(isComment)
				return dom.createComment(" " + string + " ");
			else
				return dom.createTextNode(string);
		}
		
		e = dom.createElement(this.string);
		for (int i=0;i<attributes.size();i++) {
			e.setAttribute(attributes.get(i), attributesvalues.get(i));
		}
		for (int i=0;i<children.size();i++) {
			e.appendChild((Node) children.get(i).convertToElement(dom));
		}
		}catch(Exception er){System.out.println(er);}
		return e;
	}

	public Boolean saveToXML(File file) {
	    Document dom;

	    // instance of a DocumentBuilderFactory
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    try {
	        // use factory to get an instance of document builder
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        // create instance of DOM
	        dom = db.newDocument();

	        // create the root element
	        XMLNode rootnode = new XMLNode("");
	        rootnode.add(this);
	        
	        Element rootEle = (Element) rootnode.get(0).convertToElement(dom); //Get the first child from rootnode

	        dom.appendChild(rootEle);

	        try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "yes");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	            // send DOM to file
	            tr.transform(new DOMSource(dom), 
	                                 new StreamResult(new FileOutputStream(file.getAbsolutePath())));
	            return true;

	        } catch (TransformerException te) {
	            System.out.println(te.getMessage());
	        } catch (IOException ioe) {
	            System.out.println(ioe.getMessage());
	        }
	    } catch (ParserConfigurationException pce) {
	        System.out.println("UsersXML: Error trying to instantiate DocumentBuilder " + pce);
	    }
	    return false; //Failed to write to file
	}
}
