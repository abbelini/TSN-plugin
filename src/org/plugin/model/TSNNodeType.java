package org.plugin.model;

public enum TSNNodeType {
  SWITCH,
  SCHED_HOST,
  NON_SCHED_HOST
}
