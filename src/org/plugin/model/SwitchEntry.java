package org.plugin.model;

public class SwitchEntry {
	public int queueTime;
	public Boolean queueState[] = {true, true,true,true,true, true,true,true};
	
	public SwitchEntry(int time) {
		queueTime = time;
	}
	public String QueueStateToString() {
		String t = "";
		for (int i = 0; i < 8; i++) {
			String chr = queueState[i] ? "1" : "0";
			t = t + chr;
		}
		return t;
	}
}
