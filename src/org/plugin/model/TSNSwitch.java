package org.plugin.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class TSNSwitch {
	HashMap<Integer, SwitchPort> switchMap;
	String name;
	
	public TSNSwitch(String name) {
		switchMap = new HashMap<>();
		this.name = name;
	}

	public void addPort(int port_id, String name){
		SwitchPort port = new SwitchPort(port_id, name);
		switchMap.put(port_id, port);
	}
	public void addEntry(int int_port, int entryCycle, String queueState) {
		SwitchEntry entry = new SwitchEntry(entryCycle);
		//Add to ensure the queueState is long enough
		queueState = queueState.concat("11111111");
		for (int i = 0; i < 8; i++) {
			entry.queueState[i] = queueState.charAt(i) == '1';
		}
		switchMap.get(int_port).AddEntry(entry);
	}
	public Set<Integer> getports(){
		 return switchMap.keySet();

	}
	public SwitchPort getPort(int index) {
		
		return switchMap.get(index);
	}
	public void setPort(int index, SwitchPort port ) {
		switchMap.put(index, port);
	}
	public XMLNode getXMLNode() {
		XMLNode node = new XMLNode("switch");
		node.addAttributes("name", name);
		for(Entry<Integer, SwitchPort> entry : switchMap.entrySet()) {
		    Integer key = entry.getKey();
		    SwitchPort value = entry.getValue();

		    if(value.entryList.size()==0) {continue;} //Skip emptry ports
		    XMLNode xmlPort = new XMLNode("port");
		    XMLNode scheduleNode = new XMLNode("schedule");
		    
		    
		    xmlPort.addAttributes("id", Integer.toString(key));
		    
		    int tempCycle=0;
		    for (SwitchEntry portEntry: value.getEntries()) {
		    	XMLNode xmlEntry = new XMLNode("entry");
		    	XMLNode length = new XMLNode("length");
		    	XMLNode bitvector = new XMLNode("bitvector");
		    	length.add(new XMLNode(Integer.toString(portEntry.queueTime) + "us"));
		    	bitvector.add(new XMLNode(portEntry.QueueStateToString()));
		    	xmlEntry.add(length);
		    	xmlEntry.add(bitvector);
		    	scheduleNode.add(xmlEntry);
		    	tempCycle += portEntry.queueTime; //Keep track of highest cycle time
		    	
		    	
			}
		    scheduleNode.addAttributes("cycleTime", Double.toString(tempCycle) + "us");
		    xmlPort.add(scheduleNode);
		    node.add(xmlPort);
		}
		
		
		return node;
	}
	
}
