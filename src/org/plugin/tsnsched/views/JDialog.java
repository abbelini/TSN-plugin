package org.plugin.tsnsched.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.plugin.model.FlowListEntry;
import org.plugin.model.MathOperations;
import org.plugin.model.SwitchEntry;
import org.plugin.model.SwitchPort;
import org.swtchart.Chart;
import org.swtchart.IAxis;
import org.swtchart.IBarSeries;
import org.swtchart.ISeries.SeriesType;
import org.swtchart.ISeriesLabel;
import org.swtchart.Range;

public class JDialog extends Dialog {
	private static final int INC_ID = IDialogConstants.CLIENT_ID + 1;
	private static final int DEC_ID = IDialogConstants.CLIENT_ID + 2;
	private Chart chart;
	private Composite composite;
	protected int newXPos;
	protected int newYPos;
    private static int startXPos;
    private static int startYPos;
    private SwitchPort switchPort;
	private SetupView setupView;
	private String nodeName;
	private double chart_Range;
	private final double MAX_RANGE = 5000;
	private final double MIN_RANGE = 1000;
	private final double executionTime = 10;
	
    private static int currentX;
    private static int currentY;

    private static boolean drag = false;

	/**
	 * Create the dialog.
	 * 
	 * @param parentShell
	 * @param switchPort 
	 * @param setupView 
	 */
	public JDialog(Shell parentShell, SwitchPort switchPort, SetupView setupView, String nodeName) {
		super(parentShell);
		this.switchPort = switchPort;
		this.setupView = setupView;
		this.nodeName = nodeName;
		setShellStyle(SWT.RESIZE);
	}

	/**
	 * Create contents of the dialog.
	 * @param parent
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new FillLayout(SWT.HORIZONTAL));
		List<double[]>chartList = new ArrayList<double[]>();		
		
		//Get LCM
		double[] periodsArray = new double[switchPort.entryList.size()];
		for (int i = 0; i < periodsArray.length; i++) {

			 periodsArray[i] = switchPort.entryList.get(i).queueTime;
		}
		chart_Range = MathOperations.lcm(periodsArray);
		
		// Add each queue state to the chart
		double current = 0.0;
		do{
			for(SwitchEntry entry:switchPort.entryList)
			{
				double[] z_pos = new double[8];
				double[] z_neg = new double[8];
				int time = entry.queueTime;
				Boolean[] boolstate = entry.queueState;

				for (int i = 0; i < 8; i++) {
					if (boolstate[i]) {
						z_pos[i] = time;
					} else {
						z_neg[i] = time;
					}
				}
				chartList.add(z_pos);
				chartList.add(z_neg);
				current += entry.queueTime;
			}
		}
		while(current < chart_Range);
		
		composite = new Composite(container, SWT.NONE);
		FillLayout fl_composite = new FillLayout(SWT.HORIZONTAL);
		fl_composite.marginHeight = 10;
		fl_composite.marginWidth = 10;
		fl_composite.spacing = 10;
		composite.setLayout(fl_composite);
		
		
		
		createChart(composite,chartList);
		

		return container;
	}
	private void createChart(Composite container, List<double[]> chartList) {
		// create a chart
		chart = new Chart(container, SWT.NONE);
		        
		// set titles
		chart.getTitle().setText("Gate schedule");
		chart.getAxisSet().getXAxis(0).getTitle().setText("Queue");
		chart.getAxisSet().getYAxis(0).getTitle().setText("Time (us)");

		// set category
		chart.getAxisSet().getXAxis(0).enableCategory(true);
		String[] category = {"1","2","3","4","5","6","7","8"};
		chart.getAxisSet().getXAxis(0).setCategorySeries(category);
	


	
		int i = 0;
		for ( double[] entry : chartList) {
			// create bar series
			IBarSeries barSeries = (IBarSeries) chart.getSeriesSet().createSeries(SeriesType.BAR, "1" + Integer.toString(i));
			barSeries.setYSeries(entry);
			int color = i % 2 == 0 ? SWT.COLOR_GREEN : SWT.COLOR_RED;
			barSeries.setBarColor(Display.getDefault().getSystemColor(color));
			barSeries.enableStack(true);
			barSeries.setBarPadding(10); // sets the bar padding to 50%

			
			
			ISeriesLabel seriesLabel = barSeries.getLabel();
			String[] labels = new String[entry.length];
			for (int j = 0; j < labels.length; j++) {
				if(entry[j]==0.0){
					labels[j]="  ";
				}
				else {
					labels[j]="#.###########";
				}
			}
			seriesLabel.setFormats(labels);
			seriesLabel.setVisible(true);
			i++;
		}
		
	
		chart.setOrientation(SWT.VERTICAL);
		chart.getLegend().setVisible(false);
		chart.getAxisSet().adjustRange();
		final Composite plotArea = chart.getPlotArea();
		plotArea.addListener(SWT.MouseDown, new Listener() {

            @Override
            public void handleEvent(Event event) {
                startXPos = event.x;
                startYPos = event.y;

                drag = true;
            }
        });
		plotArea.addListener(SWT.MouseUp, new Listener() {

            @Override
            public void handleEvent(Event event) {

                drag = false;
            }
        });
		plotArea.addListener(SWT.MouseMove, new Listener() {

            @Override
            public void handleEvent(Event event) {
                newXPos = event.x;
                newYPos = event.y;

            }
        });
		
	}


	
	
	
	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);

	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(1050, 800);
	}
}
