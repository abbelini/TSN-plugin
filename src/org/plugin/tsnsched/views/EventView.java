package org.plugin.tsnsched.views;


import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import java.io.File;
import java.util.ArrayList;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.plugin.model.TreeContentProvider;
import org.plugin.model.TreeLabelProvider;
import org.plugin.model.XMLNode;

/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class EventView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.plugin.tsnsched.views.EventView";
	protected TreeViewer viewer;
	public String filepath;

	
	
	

	public EventView() {
		
	}

	public void createPartControl(Composite parent) {

	    viewer = new TreeViewer(parent); 
	    viewer.setLabelProvider(new TreeLabelProvider()); 
	    viewer.setContentProvider(new TreeContentProvider()); 
	    viewer.setInput(null);
	    
	    
	    makeActions();
	    contributeToActionBars();
	}

		 	
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);

	}
	
	
	private void makeActions() {

	}


	
	
	public void readEvents(){
	    try {
			File inputFileini = new File(org.plugin.tsnsched.Activator.filepath);
			IWorkspace workspace= ResourcesPlugin.getWorkspace();
			
			IPath pathini = org.eclipse.core.runtime.Path.fromOSString(inputFileini.getAbsolutePath());
			
			IFile iFileini = workspace.getRoot().getFileForLocation(pathini);
			
			
			IEditorPart editorPart = getSite().getPage().getActiveEditor();
			
			
			

	   }
	   catch(Exception e){

	   }
	}
	
	private void displayEditPromt(XMLNode node){
		int numOfAttributes = node.attributes.size();
		Display display = PlatformUI.getWorkbench().getDisplay();
		ArrayList<Label> Labels = new ArrayList<>();
		ArrayList<Text> Texts = new ArrayList<>();
		
		
		final Shell dialog = new Shell (display, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText("Dialog Shell");
		FormLayout formLayout = new FormLayout ();
		formLayout.marginWidth = 10;
		formLayout.marginHeight = 10;
		formLayout.spacing = 10;
		dialog.setLayout (formLayout);

		FormData data = new FormData ();
		Button cancel = new Button (dialog, SWT.PUSH);
		cancel.setText ("Cancel");
		data.width = 60;
		data.right = new FormAttachment (100, 0);
		data.bottom = new FormAttachment (100, 0);
		cancel.setLayoutData (data);
		cancel.addSelectionListener (widgetSelectedAdapter(event -> {

			dialog.close ();
		}));
		
		Button ok = new Button (dialog, SWT.PUSH);
		ok.setText ("OK");
		data = new FormData ();
		data.width = 60;
		data.right = new FormAttachment (cancel, 0, SWT.DEFAULT);
		data.bottom = new FormAttachment (100, 0);
		ok.setLayoutData (data);
		ok.addSelectionListener (widgetSelectedAdapter(event -> {
	
			node.string = Texts.get(Texts.size() - 1).getText(); // Change the node based on user input
			for (int i = 0; i < numOfAttributes; i++) {
				node.attributesvalues.set(numOfAttributes - i - 1, Texts.get(i).getText());
			}
			dialog.close ();
		}));
		
		for (int i = 0; i < numOfAttributes + 1; i++) {
			//Text
			Text text = new Text (dialog, SWT.BORDER);
			data = new FormData ();
			data.width = 200;
			data.right = new FormAttachment (100, 0);
			if(i==0)
				data.bottom = new FormAttachment (cancel, 0,SWT.TOP);
			else
				data.bottom = new FormAttachment (Texts.get(i-1), 0,SWT.DEFAULT);
			text.setLayoutData (data);
			// Populate with text
			if(i == numOfAttributes){
				// This is the last, add node name/text
				text.setText(node.string);
			}
			else{
				// Add attributes in reveres order
				text.setText(node.attributesvalues.get(numOfAttributes - i - 1));
			}
			
			Texts.add(text);
			
			// Label
			Label label = new Label (dialog, SWT.NONE);
			data = new FormData ();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment (text, 0, SWT.LEFT);
			data.bottom = new FormAttachment (text, 0, SWT.CENTER);
			data.top = new FormAttachment (text, 0, SWT.CENTER);
			label.setLayoutData (data);
			if(i == numOfAttributes){
				// This is the last, add node name/text
				label.setText("Node :");
			}
			else{
				// Add attributes in reveres order
				label.setText(node.attributes.get(numOfAttributes - i - 1)+ " :");
			}
			Labels.add(label);
		}
		
		dialog.setDefaultButton (ok);
		dialog.pack ();
		dialog.open ();

		while (!dialog.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}