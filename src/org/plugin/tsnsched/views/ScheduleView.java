package org.plugin.tsnsched.views;


import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.plugin.model.TreeContentProvider;
import org.plugin.model.TreeLabelProvider;
import org.plugin.model.XMLNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class ScheduleView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.plugin.tsnsched.views.ScheduleView";
	protected TreeViewer viewer;
	public String filepath;

	private Action action1;

	private Action expandAll;
	private Action collapsAll;
	private Action doubleClickAction;
	private Action saveFile;
	
	
	

	public ScheduleView() {
	}

	public void createPartControl(Composite parent) {

	    viewer = new TreeViewer(parent); 
	    viewer.setLabelProvider(new TreeLabelProvider()); 
	    viewer.setContentProvider(new TreeContentProvider()); 
	    viewer.setInput(createModel());
	    
	    
	    makeActions();
	    contributeToActionBars();
	    hookDoubleClickAction();
	}
	private XMLNode createModel(){
		XMLNode node = new XMLNode("Empty");
		
		node.add(new XMLNode("Empty - 1"));
		node.get(0).add(new XMLNode("Empty - 1.1"));
		node.get(0).add(new XMLNode("Empty - 1.2"));
		
		node.add(new XMLNode("Empty - 2"));
		
		return node; 
	} 
		 	
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		manager.add(action1);
		manager.add(collapsAll);
		manager.add(expandAll);
		manager.add(saveFile);
	}
	
	
	private void makeActions() {
		// Reading action
		action1 = new Action() {
			public void run() {
				readXMLFile();
			}
		};
		action1.setText("Action 1");
		action1.setToolTipText("Action 1 tooltip");
		action1.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ELCL_SYNCED));
		
		// CollapsAll action
		collapsAll = new Action() {
			public void run() {
				viewer.collapseAll();
			}
		};
		collapsAll.setText("Collaps All");
		collapsAll.setToolTipText("Collaps All tooltip");
		collapsAll.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ELCL_COLLAPSEALL));
		
		// ExpandAll action
		expandAll = new Action() {
			public void run() {
				viewer.expandAll();
			}
		};
		expandAll.setText("Expand All");
		expandAll.setToolTipText("Expand All tooltip");
		expandAll.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_TOOL_UP));
		
		// Save xml file action
		saveFile = new Action() {
			public void run() {
				saveToXML();
			}
		};
		saveFile.setText("Save current tree to file");
		saveFile.setToolTipText("Save to file");
		saveFile.setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().
			getImageDescriptor(ISharedImages.IMG_ETOOL_SAVE_EDIT));
		
		
		doubleClickAction = new Action() {
			public void run() {
				ISelection selection = viewer.getSelection();
				Object obj = ((IStructuredSelection)selection).getFirstElement();
				displayEditPromt((XMLNode) obj);
				viewer.refresh();
			}
		};
	}

	private void hookDoubleClickAction() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			public void doubleClick(DoubleClickEvent event) {
				doubleClickAction.run();
			}
		});
	}
	
	public void readXMLFile(){
	    try {
			File inputFile = new File(org.plugin.tsnsched.Activator.filepath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(inputFile);
			doc.getDocumentElement().normalize();

			XMLNode root = new XMLNode("");
			if (doc.hasChildNodes()) {
				root.children = getNodes(doc.getChildNodes());
			}
			else{
				root = new XMLNode("Empty");
			}
			viewer.setInput(root);
	   }
	   catch(Exception e){
		   
	   }
	}
	
	private ArrayList<XMLNode> getNodes(NodeList nodeList){
		ArrayList<XMLNode> nodes = new ArrayList<>();
		for (int count = 0; count < nodeList.getLength(); count++) {

			Node tempNode = nodeList.item(count);
			XMLNode xmlNode = new XMLNode("");
			// make sure it's element node.
			if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

				xmlNode.string += tempNode.getNodeName();
				if (tempNode.hasAttributes()) {

					// get attributes names and values
					NamedNodeMap nodeMap = tempNode.getAttributes();

					for (int i = 0; i < nodeMap.getLength(); i++) {

						Node node = nodeMap.item(i);
						xmlNode.addAttributes(node.getNodeName(), node.getNodeValue());
						
					}
				}
				if (tempNode.hasChildNodes()) {

					// loop again if has child nodes
					
					xmlNode.children = getNodes(tempNode.getChildNodes());
				}
				
				nodes.add(xmlNode);

			}
			else if (tempNode.getNodeType() == Node.TEXT_NODE || tempNode.getNodeType() == Node.COMMENT_NODE ) {	
				String s = tempNode.getTextContent().trim();
				if(!s.isEmpty()){
					xmlNode.string = s;
					xmlNode.isComment = tempNode.getNodeType() == Node.COMMENT_NODE; // Track if this is a comment
					nodes.add(xmlNode);
				}
			}
		}
		return nodes;
	}
	
	
	private void displayEditPromt(XMLNode node){
		int numOfAttributes = node.attributes.size();
		Display display = PlatformUI.getWorkbench().getDisplay();
		ArrayList<Label> Labels = new ArrayList<>();
		ArrayList<Text> Texts = new ArrayList<>();
		
		
		final Shell dialog = new Shell (display, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText("Dialog Shell");
		FormLayout formLayout = new FormLayout ();
		formLayout.marginWidth = 10;
		formLayout.marginHeight = 10;
		formLayout.spacing = 10;
		dialog.setLayout (formLayout);

		FormData data = new FormData ();
		Button cancel = new Button (dialog, SWT.PUSH);
		cancel.setText ("Cancel");
		data.width = 60;
		data.right = new FormAttachment (100, 0);
		data.bottom = new FormAttachment (100, 0);
		cancel.setLayoutData (data);
		cancel.addSelectionListener (widgetSelectedAdapter(event -> {

			dialog.close ();
		}));
		
		Button ok = new Button (dialog, SWT.PUSH);
		ok.setText ("OK");
		data = new FormData ();
		data.width = 60;
		data.right = new FormAttachment (cancel, 0, SWT.DEFAULT);
		data.bottom = new FormAttachment (100, 0);
		ok.setLayoutData (data);
		ok.addSelectionListener (widgetSelectedAdapter(event -> {
			
			node.string = Texts.get(Texts.size() - 1).getText(); // Change the node based on user input
			for (int i = 0; i < numOfAttributes; i++) {
				node.attributesvalues.set(numOfAttributes - i - 1, Texts.get(i).getText());
				

				
				
			}
			dialog.close ();
		}));
		
		for (int i = 0; i < numOfAttributes + 1; i++) {
			//Text
			Text text = new Text (dialog, SWT.BORDER);
			data = new FormData ();
			data.width = 200;
			data.right = new FormAttachment (100, 0);
			if(i==0)
				data.bottom = new FormAttachment (cancel, 0,SWT.TOP);
			else
				data.bottom = new FormAttachment (Texts.get(i-1), 0,SWT.DEFAULT);
			text.setLayoutData (data);
			// Populate with text
			if(i == numOfAttributes){
				// This is the last, add node name/text
				text.setText(node.string);
			}
			else{
				// Add attributes in reveres order
				text.setText(node.attributesvalues.get(numOfAttributes - i - 1));
			}
			
			Texts.add(text);
			
			// Label
			Label label = new Label (dialog, SWT.NONE);
			data = new FormData ();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment (text, 0, SWT.LEFT);
			data.bottom = new FormAttachment (text, 0, SWT.CENTER);
			data.top = new FormAttachment (text, 0, SWT.CENTER);
			label.setLayoutData (data);
			if(i == numOfAttributes){
				// This is the last, add node name/text
				label.setText("Node :");
			}
			else{
				// Add attributes in reveres order
				label.setText(node.attributes.get(numOfAttributes - i - 1)+ " :");
			}
			Labels.add(label);
		}
		
		dialog.setDefaultButton (ok);
		dialog.pack ();
		dialog.open ();

		while (!dialog.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		
	}
	

	public void saveToXML() {
	    Document dom;

	    // instance of a DocumentBuilderFactory
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	    try {
	        // use factory to get an instance of document builder
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        // create instance of DOM
	        dom = db.newDocument();

	        // create the root element
	        XMLNode rootnode = (XMLNode)viewer.getInput();
	        
	        Element rootEle = (Element) rootnode.get(0).convertToElement(dom); //Get the first child from rootnode

	        dom.appendChild(rootEle);

	        try {
	            Transformer tr = TransformerFactory.newInstance().newTransformer();
	            tr.setOutputProperty(OutputKeys.INDENT, "yes");
	            tr.setOutputProperty(OutputKeys.METHOD, "xml");
	            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	            // send DOM to file
	            tr.transform(new DOMSource(dom), 
	                                 new StreamResult(new FileOutputStream(org.plugin.tsnsched.Activator.filepath + "new")));

	        } catch (TransformerException te) {

	        } catch (IOException ioe) {

	        }
	    } catch (ParserConfigurationException pce) {
	        
	    }
	}
	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}